package com.example.CourseManagement.controllers;

import com.example.CourseManagement.models.Course;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.security.Jwt.AuthTokenFilter;
import com.example.CourseManagement.services.CourseService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.print.attribute.standard.Media;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(roles = {"INSTRUCTOR"})
class InstructorControllerTest {
    @MockBean
    CourseService courseService;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;


    @BeforeEach
    void setUp() throws Exception{
        AuthTokenFilter authTokenFilter = mock(AuthTokenFilter.class);
        doNothing().when(authTokenFilter).doFilterInternal(any(HttpServletRequest.class),
                any(HttpServletResponse.class),any(FilterChain.class));
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void getInstructorCourses() throws Exception{
        User customUser = new User(
                "chris","agina",
                "chrisagina@gmail.com","password");
        Course course1 = new Course("course1", customUser);
        Course course2 = new Course("course2",customUser);
        List<Course> courseList = new ArrayList<>();
        courseList.addAll(List.of(course1,course2));
        when(courseService.findCourseForInstructor()).thenReturn(courseList);

        mvc.perform(MockMvcRequestBuilders.get("/api/instructor/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
    @Test
    void getInstructorHasNoCourses() throws Exception {
        List<Course> courseList = new ArrayList<>();
        when(courseService.findCourseForInstructor()).thenReturn(courseList);
        mvc.perform(MockMvcRequestBuilders.get("/api/instructor/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath(".message").value("The instructor has no courses"));

    }
}