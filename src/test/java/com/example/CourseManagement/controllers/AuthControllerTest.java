package com.example.CourseManagement.controllers;

import com.example.CourseManagement.payload.request.LoginRequest;
import com.example.CourseManagement.payload.response.JwtResponse;
import com.example.CourseManagement.security.Jwt.JwtUtils;
import com.example.CourseManagement.security.services.UserDetailsImpl;
import com.example.CourseManagement.security.services.UserDetailsServiceImpl;
import com.example.CourseManagement.services.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@RunWith(SpringRunner.class)

class AuthControllerTest {
    @MockBean
    AuthService authService;
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext context;
    ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void authenticateUser() throws Exception {
        List<String> strRoles =new ArrayList<>();
        strRoles.add(null);
        JwtResponse response = new JwtResponse("some-token", 1L,"chrisagina",
                "chrisagina@gmail.com",strRoles);
        LoginRequest loginRequest = new LoginRequest("chrisagina","password");
        when(authService.login(loginRequest.getUsername(), loginRequest.getPassword()))
                .thenReturn(response);

        String jsonRequest = objectMapper.writeValueAsString(loginRequest);
        mvc.perform(MockMvcRequestBuilders.post("/api/auth/signin").content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").value("some-token"));

    }
}