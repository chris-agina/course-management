package com.example.CourseManagement.controllers;

import com.example.CourseManagement.models.Course;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.security.Jwt.AuthTokenFilter;
import com.example.CourseManagement.services.CourseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(roles = {"STUDENT"})
class StudentControllerTest {
    @MockBean
    CourseService courseService;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @BeforeEach
    void setUp() throws Exception{

        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void registerCourse() throws Exception{
        AuthTokenFilter authTokenFilter = mock(AuthTokenFilter.class);
        doNothing().when(authTokenFilter).doFilterInternal(any(HttpServletRequest.class),
                any(HttpServletResponse.class),any(FilterChain.class));
        User customUser = new User(
                "chris","agina",
                "chrisagina@gmail.com","password");
        Course course1 = new Course("course1", customUser);
        when(courseService.addStudentToCourse(1L)).thenReturn(course1);
        mvc.perform(MockMvcRequestBuilders.post("/api/student/register/{id}","1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }
}