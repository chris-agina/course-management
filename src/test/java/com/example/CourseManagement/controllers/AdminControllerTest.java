package com.example.CourseManagement.controllers;
import com.example.CourseManagement.models.Course;
import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.CourseRegisterRequest;
import com.example.CourseManagement.payload.request.RegisterRequest;
import com.example.CourseManagement.repositories.UserRepository;
import com.example.CourseManagement.security.Jwt.AuthTokenFilter;
import com.example.CourseManagement.services.AuthService;
import com.example.CourseManagement.services.CourseService;
import com.example.CourseManagement.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@SpringBootTest
@WithMockUser(roles = {"ADMIN"})
class AdminControllerTest {
    @InjectMocks
    AdminController underTest;
    @MockBean
    AuthService authService;
    @MockBean
    CourseService courseService;
    @MockBean
    UserService userService;
    @MockBean
    UserRepository userRepository;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;
    ObjectMapper objectMapper;


    @BeforeEach
    void setUp() throws Exception {
        AuthTokenFilter authTokenFilter = mock(AuthTokenFilter.class);
        doNothing().when(authTokenFilter).doFilterInternal(any(HttpServletRequest.class),
                any(HttpServletResponse.class),any(FilterChain.class));

        mvc = MockMvcBuilders.webAppContextSetup(context).build();
        objectMapper = new ObjectMapper();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void registerStudentWhenRoleIsNull() throws Exception{
        RegisterRequest registerRequest = new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(post("/api/admin/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(".message").value("User Registered Successfully"));
    }
    @Test
    void registerAllRoles() throws Exception{
        RegisterRequest registerRequest = new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        String adminRole = "admin";
        String  instructorRole = "instructor";
        Set<String>roles= new HashSet<>();
        roles.addAll(List.of(adminRole,instructorRole));
        registerRequest.setRole(roles);
        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(post("/api/admin/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(".message").value("User Registered Successfully"));
    }
    @Test
    void registerDefault() throws Exception{
        RegisterRequest registerRequest = new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        String defaultRole = "default";
        Set<String>roles= new HashSet<>();
        roles.add(defaultRole);
        registerRequest.setRole(roles);
        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(post("/api/admin/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(".message").value("User Registered Successfully"));
    }

    @Test
    void registerStudentThrowsErrorOnUser() throws Exception{
        RegisterRequest registerRequest = new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        given(userRepository.existsByusername(registerRequest.getUsername()))
                .willReturn(true);
        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(post("/api/admin/register")
                .content(jsonRequest)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }
    @Test
    void registerStudentThrowsErrorOnEmail() throws Exception{
        RegisterRequest registerRequest = new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        given(userRepository.existsByEmail(registerRequest.getEmail()))
                .willReturn(true);
        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(post("/api/admin/register")
                        .content(jsonRequest)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    void getLoggedInUser() throws Exception{

        User customUser = new User(
                "chris","agina",
                "chrisagina@gmail.com","password");


        when(userService.getLoggedInUserDetails()).thenReturn(Optional.of(customUser));

        mvc.perform(get("/api/admin/details")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("chrisagina"));

    }

    @Test
    void getUsers() throws Exception{

        mvc.perform(get("/api/admin/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }


    @Test
    void updateUsers() throws  Exception{
        User customUser = new User(
                "chris","agina",
                "chrisagina@gmail.com","password");
        RegisterRequest registerRequest =new RegisterRequest(
                "chris","agina","chrisagina@gmail.com","password");
        when(userService.updateUser(registerRequest)).thenReturn(customUser);


        String jsonRequest = objectMapper.writeValueAsString(registerRequest);
        mvc.perform(put("/api/admin/update").content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void deleteUser() throws Exception {


        mvc.perform(delete("/api/admin/delete/{id}","1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void addCourse() throws Exception {
        CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1",1L);
        String jsonRequest = objectMapper.writeValueAsString(courseRegisterRequest);
        mvc.perform(post("/api/admin/courses/").content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Course added successfully"));

    }

    @Test
    void getCourses() throws Exception {

        mvc.perform(get("/api/admin/courses")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void updateCourse() throws Exception{
        CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1",1L);
        String jsonRequest = objectMapper.writeValueAsString(courseRegisterRequest);
        mvc.perform(put("/api/admin/courses/{id}","1").content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}