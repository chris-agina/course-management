package com.example.CourseManagement.services;

import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.RegisterRequest;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserService underTest;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    UserDetails userDetails;
    @Mock
    SecurityContextHolder securityContextHolder;
    @Mock
    SecurityContext  securityContext;
    @Mock
    Authentication authentication;
    @Spy
    PasswordEncoder passwordEncoder;



    @BeforeEach
    void setUp(){
        underTest = new UserService(userRepository,roleRepository,passwordEncoder);

    }
    @Test
    void getUsers() {
    //when
        underTest.getUsers();

    //then
    verify(userRepository).findAll();
    }

    @Test
    void getLoggedInUserDetailsTest() {
        //given
        Optional<User>  user = Optional.of(mock(User.class));

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
        when(userRepository.findByusername(userDetails.getUsername())).thenReturn(user);
        //when
       Optional<User> actual = underTest.getLoggedInUserDetails();
        //then
        assertEquals(user,actual);

    }

    @Test
    void updateUser() {
        //given
        User user = new User();
        Role adminRole = mock(Role.class);
        Role studentRole = mock(Role.class);
        Role instructorRole = mock(Role.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
        when(underTest.getLoggedInUserDetails()).thenReturn(Optional.of(user));
        when(roleRepository.findByName(ERole.ROLE_ADMIN)).thenReturn(Optional.of(adminRole));
        when(roleRepository.findByName(ERole.ROLE_STUDENT)).thenReturn(Optional.of(studentRole));
        when(roleRepository.findByName(ERole.ROLE_INSTRUCTOR)).thenReturn(Optional.of(instructorRole));
        RegisterRequest registerRequest = new RegisterRequest("chris","agina",
                "chrisagina@gmail.com","pass");
        Set<String> strRoles = new HashSet<>();
        strRoles.addAll(List.of("admin","student","instructor"));
        registerRequest.setRole(strRoles);

        underTest.updateUser(registerRequest);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User capturedUser = userArgumentCaptor.getValue();

        User expected = new User("chris","agina","chrisagina@gmail.com","pass");
        Set<Role> roles = new HashSet<>();
        roles.addAll(List.of(adminRole,instructorRole,studentRole));
        expected.setFirstName("chris");
        expected.setLastName("agina");
        expected.setEmail("chrisagina@gmail.com");
        expected.setPassword(passwordEncoder.encode("pass"));
        expected.setRoles(roles);



        assertEquals(expected,capturedUser);





    }
    @Test
    void updateUserDefaultCase(){
        //given
        User user = mock(User.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
        when(underTest.getLoggedInUserDetails()).thenReturn(Optional.of(user));
        RegisterRequest registerRequest = new RegisterRequest("chris","agina",
                "chrisagina@gmail.com","pass");
        Set<String> strRoles = new HashSet<>();
        strRoles.addAll(List.of("wrongRole"));
        registerRequest.setRole(strRoles);
//        User actual = underTest.updateUser(registerRequest);
        assertThrows(RuntimeException.class,()->underTest.updateUser(registerRequest));



    }

    @Test
    void deleteUser() {
        //given
        Long userId = Long.valueOf(1);
        given(userRepository.existsById(Long.valueOf(1)))
                .willReturn(true);

        //when
        underTest.deleteUser(userId);

        //then
        verify(userRepository).deleteById(userId);
    }
    @Test
    void throwsIllegalStateException(){
        //given
        Long userId = Long.valueOf(1);

        assertThrows(IllegalStateException.class, ()->underTest.deleteUser(userId));
    }
}