package com.example.CourseManagement.services;

import com.example.CourseManagement.payload.request.LoginRequest;
import com.example.CourseManagement.payload.response.JwtResponse;
import com.example.CourseManagement.repositories.UserRepository;
import com.example.CourseManagement.security.Jwt.JwtUtils;
import com.example.CourseManagement.security.services.UserDetailsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {
    @InjectMocks
    AuthService underTest;
    @Mock
    AuthenticationManager authenticationManager;
    @Mock
    JwtUtils jwtUtils;

    @BeforeEach
    void setUp() {

    }

    @Test
    void login() {
        String password = "password";
        String username = "chrsagina";
        Authentication authentication = mock(Authentication.class);
        List<String> strRoles =new ArrayList<>();
        strRoles.add(null);


        GrantedAuthority grantedAuthority =mock(GrantedAuthority.class);
        Collection<GrantedAuthority> authority = new HashSet<>();
        authority.add(grantedAuthority);


        UserDetailsImpl userDetails = new UserDetailsImpl(1L,"chrisagina",
                "chrisagina@gmail.com","password",authority);
        when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password)))
                .thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(jwtUtils.generateJwtToken(authentication)).thenReturn("some-token");


      JwtResponse actual = underTest.login(username,password);
      JwtResponse expected = new JwtResponse("some-token", 1L,"chrisagina",
              "chrisagina@gmail.com",strRoles);

      assertEquals(expected,actual);



    }
}