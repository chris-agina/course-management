package com.example.CourseManagement.services;

import com.example.CourseManagement.models.Course;
import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.CourseRegisterRequest;
import com.example.CourseManagement.repositories.CourseRepository;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class CourseServiceTest {

    @Mock
    CourseRepository courseRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    RoleRepository roleRepository;
    @InjectMocks
    CourseService underTest;
   @Mock
   UserDetails userDetails;
   @Mock
   SecurityContextHolder securityContextHolder;
   @Mock
   SecurityContext securityContext;
   @Mock
   Authentication authentication;

    @BeforeEach
    void setUp() {
        underTest = new CourseService(courseRepository,userRepository,roleRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getCoursesTest() {
        //when
        underTest.getCourses();
        //then
        verify(courseRepository).findAll();
    }

    @Test
    void addCourse() {
       //given
       User instructor = mock(User.class);
       Role role = mock(Role.class);
       Course course = new Course("course1",instructor);
       Set<Role> roles = new HashSet<>();
       roles.add(role);
       instructor.setRoles(roles);
       CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1",instructor.getId());
       when(userRepository.findById(courseRegisterRequest.getInstructorId())).thenReturn(Optional.of(instructor));
       when(roleRepository.findByName(ERole.ROLE_INSTRUCTOR)).thenReturn(Optional.of(role));
       when(instructor.getRoles()).thenReturn(roles);




       //when
       underTest.addCourse(courseRegisterRequest);

       //then
       ArgumentCaptor<Course> courseArgumentCaptor = ArgumentCaptor.forClass(Course.class);

       verify(courseRepository).save(courseArgumentCaptor.capture());
       Course capturedCourse = courseArgumentCaptor.getValue();
       assertThat(capturedCourse).isEqualTo(course);



    }
    @Test
    void throwsIllegalStateExceptionOcCourse(){
       //given
       User instructor = mock(User.class);
       Role role = mock(Role.class);
       Course course = new Course("course1",instructor);
       Long courseId = Long.valueOf(1);
       Set<Role> roles = new HashSet<>();
       roles.add(role);
       instructor.setRoles(roles);
       CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1",instructor.getId());
       when(userRepository.findById(courseRegisterRequest.getInstructorId())).thenReturn(Optional.of(instructor));
       when(roleRepository.findByName(ERole.ROLE_INSTRUCTOR)).thenReturn(Optional.of(role));
       when(instructor.getRoles()).thenReturn(roles);
       when(courseRepository.findByName(courseRegisterRequest.getName())).thenReturn(Optional.of(course));
       when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));


       //then
       assertThrows(IllegalStateException.class,()->underTest.addCourse(courseRegisterRequest),
               "course already exists");
       assertThrows(IllegalStateException.class,()->underTest.updateCourse(courseRegisterRequest,courseId),
               "course already exists");

    }
    @Test
    void throwsIllegalStateExceptionOnUser(){
       User user = mock(User.class);
       Long courseId = Long.valueOf(1);
       Role role = mock(Role.class);
       Set<Role> roles = new HashSet<>();
       roles.add(role);

       CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1", user.getId());

       when(userRepository.findById(courseRegisterRequest.getInstructorId())).thenReturn(Optional.of(user));
       when(roleRepository.findByName(ERole.ROLE_INSTRUCTOR)).thenReturn(Optional.of(role));
        when(roleRepository.findByName(ERole.ROLE_STUDENT)).thenReturn(Optional.of(role));
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
        when(userRepository.findByusername(userDetails.getUsername())).thenReturn(Optional.of(user));

       assertThrows(IllegalStateException.class,()->underTest.addCourse(courseRegisterRequest),
               "User is not an instructor");
       assertThrows(IllegalStateException.class,()->underTest.updateCourse(courseRegisterRequest,courseId),
               "The chosen user is not an instructor");
       assertThrows(IllegalStateException.class,()->underTest.addStudentToCourse(courseId),
               "The chosen user is not a student");


    }

    @Test
    void updateCourse() {
       //given
       User instructor = mock(User.class);
       Role role = mock(Role.class);
       Course course = mock(Course.class);
       Long courseId = Long.valueOf(1);
       Set<Role> roles = new HashSet<>();
       roles.add(role);
       instructor.setRoles(roles);
       CourseRegisterRequest courseRegisterRequest = new CourseRegisterRequest("course1",instructor.getId());


       //when
       when(userRepository.findById(courseRegisterRequest.getInstructorId())).thenReturn(Optional.of(instructor));
       when(roleRepository.findByName(ERole.ROLE_INSTRUCTOR)).thenReturn(Optional.of(role));
       when(instructor.getRoles()).thenReturn(roles);
       when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
       underTest.updateCourse(courseRegisterRequest,courseId);

       //then
       ArgumentCaptor<Course> courseArgumentCaptor = ArgumentCaptor.forClass(Course.class);
       verify(courseRepository).save(courseArgumentCaptor.capture());
       Course capturedCourse = courseArgumentCaptor.getValue();
       assertEquals(course,capturedCourse);


    }

    @Test
    void addStudentToCourse() {
        Long courseId = Long.valueOf(1);
       User student = mock(User.class);
       Role studentRole = mock(Role.class);
       Course course = mock(Course.class);
        Set<Role> roles = new HashSet<>();
        roles.add(studentRole);
        student.setRoles(roles);
        Set<User> courseUser = new HashSet<>();
        courseUser.add(student);
        course.setStudents(courseUser);

        when(roleRepository.findByName(ERole.ROLE_STUDENT)).thenReturn(Optional.of(studentRole));
       when(securityContext.getAuthentication()).thenReturn(authentication);
       SecurityContextHolder.setContext(securityContext);
       when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
       when(userRepository.findByusername(userDetails.getUsername())).thenReturn(Optional.of(student));
       when(student.getRoles()).thenReturn(roles);
       when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
       Course actual = underTest.addStudentToCourse(courseId);

       //then
        assertEquals(course,actual);


    }

    @Test
    void findCourseForInstructor() {
        //given
        User instructor = mock(User.class);
        Course course1 = mock(Course.class);
        List<Course> courseList = new ArrayList<>();
        courseList.add(course1);
        //when
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userDetails);
        when(userRepository.findByusername(userDetails.getUsername())).thenReturn(Optional.of(instructor));
        when(courseRepository.findByInstructorId(instructor.getId())).thenReturn(courseList);
       List<Course> actual = underTest.findCourseForInstructor();
       //then
        assertEquals(courseList,actual);


    }
}