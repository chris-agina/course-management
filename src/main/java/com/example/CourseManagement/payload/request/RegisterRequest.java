package com.example.CourseManagement.payload.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class RegisterRequest {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String username;


    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<String> role;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
    public Set<String> getRole() {
        return this.role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }
    public void setUsername(String firstName,String lastName) {
        this.username = firstName+lastName;
    }
    public RegisterRequest(String firstName,String lastName, String email, String password){
        this.firstName = firstName;
        this.lastName = lastName;
        setUsername(firstName,lastName);
        this.email = email;
        this.password = password;
    }
}
