package com.example.CourseManagement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class CourseRegisterRequest {
    @NotBlank
    private String name;
    @NotBlank
    private Long instructorId;
}
