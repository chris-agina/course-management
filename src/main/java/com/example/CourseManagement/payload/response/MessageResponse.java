package com.example.CourseManagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class MessageResponse {
    public String message;
}
