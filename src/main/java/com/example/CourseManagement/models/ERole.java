package com.example.CourseManagement.models;

public enum ERole {
    ROLE_STUDENT,
    ROLE_INSTRUCTOR,
    ROLE_ADMIN
}
