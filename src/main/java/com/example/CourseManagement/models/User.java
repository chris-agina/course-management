package com.example.CourseManagement.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
@Data
@NoArgsConstructor
public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        @NotBlank
        private String username;
        @NotBlank
        private String firstName;
        @NotBlank
        private String lastName;
        @NotBlank
        @Size(max = 50)
        @Email
        private String email;
        @NotBlank
        @Size(max = 120)
        private String password;

        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(name = "user_roles",
                joinColumns = @JoinColumn(name = "user_id"),
                inverseJoinColumns = @JoinColumn(name = "role_id"))
        private Set<Role> roles = new HashSet<>();
        public User(String firstName,String lastName, String email, String password){
                this.firstName = firstName;
                this.lastName = lastName;
                setUsername();
                this.email = email;
                this.password = password;
        }
        public Set<Role> getRoles() {
                return roles;
        }
        public void setRoles(Set<Role> roles) {
                this.roles = roles;
        }

        public void setUsername() {
                this.username = firstName.toLowerCase()+lastName.toLowerCase();
        }
}
