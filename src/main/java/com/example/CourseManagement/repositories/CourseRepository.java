package com.example.CourseManagement.repositories;

import com.example.CourseManagement.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course,Long> {

    Optional<Course> findByName(String name);

    @Query("select c from Course c where c.instructorId.id = ?1")
    List<Course> findByInstructorId(Long id);

}
