package com.example.CourseManagement.repositories;

import com.example.CourseManagement.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByusername(String username);
    Boolean existsByusername(String username);
    Boolean existsByEmail(String email);


}
