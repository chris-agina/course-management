package com.example.CourseManagement.config;

import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
public class AdminConfig {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Bean
    CommandLineRunner commandLineRunner(){

        return args -> {
            Role role1 = new Role(ERole.ROLE_STUDENT);
            Role role2 = new Role(ERole.ROLE_INSTRUCTOR);
            Role role3 = new Role(ERole.ROLE_ADMIN);
            roleRepository.saveAll(List.of(role1,role2,role3));

            User user = new User(
                   "Chris",
                   "Agina",
                   "chrisagina@gmail.com",
                    passwordEncoder.encode("password")

            );

            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Role not found"));
            Role instructorRole = roleRepository.findByName(ERole.ROLE_INSTRUCTOR)
                    .orElseThrow(() -> new RuntimeException("Role not found"));
            Set<Role> roles = new HashSet<>(List.of(adminRole, instructorRole));
            user.setRoles(roles);
            userRepository.save(user);

        };
    }
}
