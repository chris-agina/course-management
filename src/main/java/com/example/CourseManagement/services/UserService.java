package com.example.CourseManagement.services;

import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.RegisterRequest;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Slf4j
@Service
@AllArgsConstructor
public class UserService {
@Autowired
    private final UserRepository userRepository;
@Autowired
    RoleRepository roleRepository;
@Autowired
    PasswordEncoder passwordEncoder;
    public List<User> getUsers(){
        return userRepository.findAll();}

    public Optional<User> getLoggedInUserDetails(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        return userRepository.findByusername(userDetails.getUsername());
    }
    public User updateUser(RegisterRequest registerRequest){
        User user = getLoggedInUserDetails().get();
        user.setFirstName(registerRequest.getFirstName());
        user.setLastName(registerRequest.getLastName());
        user.setEmail(registerRequest.getEmail());
        user.setUsername();
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        Set<String> strRoles = registerRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if(strRoles != null){
            strRoles.forEach(role -> {
                role = role.toLowerCase();
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Role not found."));
                        roles.add(adminRole);
                        break;
                    case "instructor":
                        Role instructorRole = roleRepository.findByName(ERole.ROLE_INSTRUCTOR)
                                .orElseThrow(() -> new RuntimeException("Role not found."));
                        roles.add(instructorRole);
                        break;
                    case "student":
                        Role studentRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                                .orElseThrow(() -> new RuntimeException("Role not found."));
                        roles.add(studentRole);
                        break;
                    default:
                       throw new RuntimeException("Role not found");
                }
            });
            user.setRoles(roles);
        }

        return userRepository.save(user);
    }
public void deleteUser(Long userId){
    if(!userRepository.existsById(userId)){
        throw new IllegalStateException("User with Id " + userId +" does not exist.");
    }
    userRepository.deleteById(userId);

}
}
