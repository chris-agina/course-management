package com.example.CourseManagement.services;

import com.example.CourseManagement.models.Course;
import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.CourseRegisterRequest;
import com.example.CourseManagement.repositories.CourseRepository;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class CourseService {
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    public List<Course> getCourses(){
        return courseRepository.findAll();
    }

    public void addCourse(CourseRegisterRequest courseRegisterRequest){
        User instructor =  userRepository.findById(courseRegisterRequest.getInstructorId())
                .orElseThrow(()-> new IllegalStateException("Instructor with the given id not found"));

        Role instructorRole = roleRepository.findByName(ERole.ROLE_INSTRUCTOR)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        Set<Role> roles = new HashSet<>();
        roles.add(instructorRole);
        if (instructor.getRoles().equals(roles)){
            Course course = new Course(courseRegisterRequest.getName(), instructor);
            Optional<Course> courseOptional = courseRepository.findByName(course.getName());
            if(courseOptional.isPresent()){
                throw new IllegalStateException("course already exists");
            }
            courseRepository.save(course);
        }else {

            throw new IllegalStateException("User is not an instructor");
        }


    }
    public Course updateCourse(CourseRegisterRequest courseRegisterRequest,Long  courseId){
        User instructor =  userRepository.findById(courseRegisterRequest.getInstructorId())
                .orElseThrow(()-> new IllegalStateException("User not found"));

        Role instructorRole = roleRepository.findByName(ERole.ROLE_INSTRUCTOR)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        Set<Role> roles = new HashSet<>();
        roles.add(instructorRole);
        if (instructor.getRoles().equals(roles)){
            Course course = new Course(courseRegisterRequest.getName(), instructor);
            Course oldCourse = courseRepository.findById(courseId).get();
            Optional<Course> courseOptional = courseRepository.findByName(course.getName());
            if(courseOptional.isPresent()){
                throw new IllegalStateException("course already exists");
            }
            oldCourse.setName(course.getName());
            oldCourse.setInstructorId(course.getInstructorId());
           return courseRepository.save(oldCourse);
        }else {

            throw new IllegalStateException("The chosen user is not an instructor");
        }

    }
    public Course addStudentToCourse(Long courseId){
        //find logged in student
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User student = userRepository.findByusername(userDetails.getUsername()).get();

        //confirm user is Student
        Role studentRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        Set<Role> roles = new HashSet<>();
        roles.add(studentRole);
        if(student.getRoles().equals(roles)){
            Course course = courseRepository.findById(courseId).get();
            Set<User> courseUser = new HashSet<>();
            courseUser.add(student);
            course.setStudents(courseUser);
            courseRepository.save(course);
            return course;
        }else {

            throw new IllegalStateException("The chosen user is not a student");
        }

    }

    public List<Course> findCourseForInstructor(){
        //find logged in user
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User instructor = userRepository.findByusername(userDetails.getUsername()).get();
        return courseRepository.findByInstructorId(instructor.getId());

    }

}
