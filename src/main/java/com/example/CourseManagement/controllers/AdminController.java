package com.example.CourseManagement.controllers;

import com.example.CourseManagement.models.ERole;
import com.example.CourseManagement.models.Role;
import com.example.CourseManagement.models.User;
import com.example.CourseManagement.payload.request.CourseRegisterRequest;
import com.example.CourseManagement.payload.request.RegisterRequest;
import com.example.CourseManagement.payload.response.MessageResponse;
import com.example.CourseManagement.repositories.RoleRepository;
import com.example.CourseManagement.repositories.UserRepository;
import com.example.CourseManagement.services.CourseService;
import com.example.CourseManagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/admin/")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserService userService;
    @Autowired
    CourseService courseService;


    @PostMapping("register")
    public ResponseEntity<?> registerStudent(@Valid @RequestBody RegisterRequest registerRequest) {
        if (userRepository.existsByusername(registerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: User name is already taken"));
        }
        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already taken"));
        }
        //Create new user's account
        User user = new User(registerRequest.getFirstName(), registerRequest.getLastName(),
                registerRequest.getEmail(), passwordEncoder.encode(registerRequest.getPassword()));
        Set<String> strRoles = registerRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null) {
            Role studentRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                    .orElseThrow(() -> new RuntimeException("Role not found"));
            roles.add(studentRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Role not found."));
                        roles.add(adminRole);
                        break;
                    case "instructor":
                        Role instructorRole = roleRepository.findByName(ERole.ROLE_INSTRUCTOR)
                                .orElseThrow(() -> new RuntimeException("Role not found."));
                        roles.add(instructorRole);
                        break;
                    default:
                        Role studentRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                                .orElseThrow(() -> new RuntimeException("Role not found"));
                        roles.add(studentRole);
                }
            });

        }

        user.setRoles(roles);
        userRepository.save(user);
        return ResponseEntity.ok(new MessageResponse("User Registered Successfully"));

    }

    @GetMapping("/details")
    public ResponseEntity<?> getLoggedInUser() {
        Optional<User> user = userService.getLoggedInUserDetails();
        return ResponseEntity.ok(user);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateUsers(@Valid @RequestBody RegisterRequest registerRequest) {
        return ResponseEntity.ok(userService.updateUser(registerRequest));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok(new MessageResponse("User deleted successfully"));
    }

    @PostMapping("/courses")
    public ResponseEntity<?> addCourse(@Valid @RequestBody CourseRegisterRequest courseRegisterRequest) {
        courseService.addCourse(courseRegisterRequest);
        return ResponseEntity.ok(new MessageResponse("Course added successfully"));
    }

    @GetMapping("/courses")
    public ResponseEntity<?> getCourses() {

        return ResponseEntity.ok(courseService.getCourses());
    }
    @PutMapping("/courses/{id}")
    public ResponseEntity<?> updateCourse(@PathVariable("id") Long courseId,@Valid @RequestBody CourseRegisterRequest courseRegisterRequest){
        return ResponseEntity.ok(courseService.updateCourse(courseRegisterRequest,courseId));
    }
}