package com.example.CourseManagement.controllers;


import com.example.CourseManagement.payload.response.MessageResponse;
import com.example.CourseManagement.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/instructor/")
@PreAuthorize("hasRole('INSTRUCTOR')")
public class InstructorController {

    @Autowired
    CourseService courseService;


    @GetMapping("/courses")
    public ResponseEntity<?> getInstructorCourses(){
        if(courseService.findCourseForInstructor().isEmpty()){
            return ResponseEntity.ok()
                    .body( new MessageResponse("The instructor has no courses"));
        }else
            return ResponseEntity.ok(courseService.findCourseForInstructor());

    }
}
