package com.example.CourseManagement.controllers;


import com.example.CourseManagement.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/student/")
@PreAuthorize("hasRole('STUDENT')")
public class StudentController {

    @Autowired
    CourseService courseService;

    @PostMapping("/register/{id}")
    public ResponseEntity<?> registerCourse(@PathVariable("id") Long courseId){

        return  ResponseEntity.ok(courseService.addStudentToCourse(courseId));
    }
}
